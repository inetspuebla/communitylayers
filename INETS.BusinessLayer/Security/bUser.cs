﻿#region Descripcion
/*
    Clase       :   BLUser
    Objetivo    :   Capa de Negocios para manejo de usuarios de segurirad INETS
    Creador     :   Luis Vázquez C
    Fecha       :   06/09/2020
    Modificado  :   
    FecMod      :   
*/
#endregion

namespace INETS.BusinessLayer.Security
{
    using INETS.Models.Security;
    #region using
    using System;


    #endregion

    public class BLUser
    {
        #region Object Constructor
        public BLUser() { }
        #endregion

        #region Public Methods
        public User GetUserByID(Guid sAppId, Guid sUserID)
        {
            try
            {
                return DlUser.GetUserByID(sAppId, sUserID);
            }
            catch (Exception ex)
            {
                TEventWin.WriteError(ex);
                return new User();
            }
        }
        #endregion

        #region ComunityLayers
        private INETS.DataLayer.Security.DLUser DlUser
        {
            get
            {
                if (dlUser == null)
                    dlUser = new DataLayer.Security.DLUser();
                return dlUser;
            }
        }
        private INETS.DataLayer.Security.DLUser dlUser;

        private INETS.ToolsLayer.Security.EventWin TEventWin
        {
            get
            {
                if (tEventWin == null)
                    tEventWin = new INETS.ToolsLayer.Security.EventWin("Log Value");
                return tEventWin;
            }
        }

        private INETS.ToolsLayer.Security.EventWin tEventWin;
        #endregion

    }
}
