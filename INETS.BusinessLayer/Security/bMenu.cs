﻿namespace INETS.BusinessLayer.Security
{
    using INETS.Models.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class BLMenu
    {
        #region Object Constructor
        public BLMenu() { }
        #endregion

        #region Public Methods
        public List<Menu> GetMenuByUser(Guid sAppId, Guid sUserID)
        {
            try
            {
                return DlMenu.GetMenuByUser(sAppId, sUserID);
            }
            catch (Exception ex)
            {
                TEventWin.WriteError(ex);
                return new List<Menu>();
            }
        }
        #endregion

        #region ComunityLayers
        private INETS.DataLayer.Security.DMenu DlMenu
        {
            get
            {
                if (dlMenu == null)
                    dlMenu = new DataLayer.Security.DMenu();
                return dlMenu;
            }
        }
        private INETS.DataLayer.Security.DMenu dlMenu;

        private INETS.ToolsLayer.Security.EventWin TEventWin
        {
            get
            {
                if (tEventWin == null)
                    tEventWin = new INETS.ToolsLayer.Security.EventWin("Log Value");
                return tEventWin;
            }
        }

        private INETS.ToolsLayer.Security.EventWin tEventWin;
        #endregion
    }
}