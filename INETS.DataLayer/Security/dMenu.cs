﻿namespace INETS.DataLayer.Security
{
    #region Using
    using INETS.Models.Security;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    #endregion

    public class DMenu
    {
        private readonly string _sConnSQL = INETS.ToolsLayer.Helper.GetConnString("Seguridad");

        #region Object Constructor
        public DMenu() { }
        #endregion

        #region Public Methods
        public List<Menu> GetMenuByUser(Guid sAppId, Guid sUserID)
        {
            Menu clsMenu;
            List<Menu> lsMenu = new List<Menu>();
            try
            {
                string strQuery = "get_MenuByUser";

                SqlDataReader dataReader = null;
                SqlParameter pAppId = new SqlParameter("@AppId", sAppId);
                SqlParameter pUserID = new SqlParameter("@UserID", sUserID);
                dataReader = DaAccess.ExecuteReader(strQuery, new SqlParameter[] { pAppId, pUserID }, System.Data.CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    clsMenu = new Menu();

                    clsMenu.IdMenu = dataReader["IdMenu"] == DBNull.Value ? int.MinValue : int.Parse(dataReader["IdMenu"].ToString());
                    clsMenu.Title = dataReader["Title"] == DBNull.Value ? string.Empty : dataReader["Title"].ToString();
                    clsMenu.Description = dataReader["Description"] == DBNull.Value ? string.Empty : dataReader["Description"].ToString();
                    clsMenu.URL = dataReader["URL"] == DBNull.Value ? string.Empty : dataReader["URL"].ToString();
                    clsMenu.Controller = dataReader["Controller"] == DBNull.Value ? string.Empty : dataReader["Controller"].ToString();
                    clsMenu.View = dataReader["View"] == DBNull.Value ? string.Empty : dataReader["View"].ToString();
                    clsMenu.Public = dataReader["Public"] == DBNull.Value ? false : bool.Parse(dataReader["Public"].ToString());
                    clsMenu.IdMenuParent = dataReader["IdMenuParent"] == DBNull.Value ? int.MinValue : int.Parse(dataReader["IdMenuParent"].ToString());
                    clsMenu.Identifier = dataReader["Identifier"] == DBNull.Value ? string.Empty : dataReader["Identifier"].ToString();
                    clsMenu.IsHeader = dataReader["IsHeader"] == DBNull.Value ? false : bool.Parse(dataReader["IsHeader"].ToString());
                    clsMenu.IsActive = dataReader["IsActive"] == DBNull.Value ? false : bool.Parse(dataReader["IsActive"].ToString());
                    clsMenu.IsMVC = dataReader["IsMVC"] == DBNull.Value ? false : bool.Parse(dataReader["IsMVC"].ToString());

                    lsMenu.Add(clsMenu);
                }
                dataReader.Close();
                DaAccess.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lsMenu;
        }
        #endregion

        #region dbAccess
        private INETS.DataAccess.DbAccessSQL DaAccess
        {
            get
            {
                if (daAccess == null)
                    daAccess = new DataAccess.DbAccessSQL(_sConnSQL);
                return daAccess;
            }
        }
        private INETS.DataAccess.DbAccessSQL daAccess;
        #endregion
    }
}
