﻿#region Descripcion
/*
    Clase       :   DLUser
    Objetivo    :   Capa de Datos para manejo de usuarios de segurirad INETS
    Creador     :   Luis Vázquez C
    Fecha       :   05/09/2020
    Modificado  :   
    FecMod      :   
*/
#endregion

namespace INETS.DataLayer.Security
{
    #region Using
    using INETS.Models.Security;
    using System;
    using System.Data.SqlClient;
    #endregion

    public class DLUser
    {
        private readonly string _sConnSQL = INETS.ToolsLayer.Helper.GetConnString("Seguridad");

        #region Object Constructor
        public DLUser() { }
        #endregion

        #region Public Methods
        public User GetUserByID(Guid sAppId, Guid sUserID)
        {
            User clsUser;

            try
            {
                string strQuery = "getUserByID";

                SqlDataReader dataReader = null;
                SqlParameter pAppId = new SqlParameter("@AppId", sAppId);
                SqlParameter pUserID = new SqlParameter("@UserID", sUserID);
                dataReader = DaAccess.ExecuteReader(strQuery, new SqlParameter[] { pAppId, pUserID }, System.Data.CommandType.StoredProcedure);

                dataReader.Read();

                clsUser = new User();

                clsUser.IdGuid = dataReader["IdUser"] == DBNull.Value ? Guid.Empty : Guid.Parse(dataReader["IdUser"].ToString());
                clsUser.IdUser = dataReader["sUser"] == DBNull.Value ? string.Empty : dataReader["sUser"].ToString();
                clsUser.Name = dataReader["sName"] == DBNull.Value ? string.Empty : dataReader["sName"].ToString();
                clsUser.Password = dataReader["sPassword"] == DBNull.Value ? string.Empty : dataReader["sPassword"].ToString();
                clsUser.IdCreatedBy = dataReader["IdCreatedBy"] == DBNull.Value ? Guid.Empty : Guid.Parse(dataReader["IdCreatedBy"].ToString());
                clsUser.CreationDate = dataReader["dCreationDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["dCreationDate"].ToString());
                clsUser.LastAccesDate = dataReader["dLastAccesDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["dLastAccesDate"].ToString());
                clsUser.Locked = dataReader["bLocked"] == DBNull.Value ? false : Convert.ToBoolean(dataReader["bLocked"].ToString());
                clsUser.Active = dataReader["bActive"] == DBNull.Value ? false : Convert.ToBoolean(dataReader["bActive"].ToString());
                clsUser.LastChangeDate = dataReader["dLastAccesDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dataReader["dLastAccesDate"].ToString());
                clsUser.AccessErrorCounter = dataReader["iAccessErrorCounter"] == DBNull.Value ? int.MinValue : Convert.ToInt32(dataReader["iAccessErrorCounter"].ToString());
                clsUser.Comments = dataReader["sComments"] == DBNull.Value ? string.Empty : dataReader["sComments"].ToString();
                clsUser.IsAdmin = dataReader["bIsAdmin"] == DBNull.Value ? false : Convert.ToBoolean(dataReader["bIsAdmin"].ToString());
                clsUser.SuperAdmin = dataReader["bIsSuperAdmin"] == DBNull.Value ? false : Convert.ToBoolean(dataReader["bIsSuperAdmin"].ToString());
                clsUser.ChangePassword = dataReader["bChangePassword"] == DBNull.Value ? false : Convert.ToBoolean(dataReader["bChangePassword"].ToString());


                dataReader.Close();
                DaAccess.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return clsUser;
        }
        #endregion

        #region dbAccess
        private INETS.DataAccess.DbAccessSQL DaAccess
        {
            get
            {
                if (daAccess == null)
                    daAccess = new DataAccess.DbAccessSQL(_sConnSQL);
                return daAccess;
            }
        }
        private INETS.DataAccess.DbAccessSQL daAccess;
        #endregion
    }
}
