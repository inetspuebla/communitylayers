﻿#region Descripcion
/*
    Clase       :   ImportExcelToCSV
    Objetivo    :   Capa de Herramientas para imoortación de archvos Excel y exportación a archivos CSV
    Creador     :   Luis Vázquez C
    Fecha       :   27/08/2020
    Modificado  :   Luis Vázquez C
    FecMod      :   27/08/2020
*/
#endregion

using ExcelDataReader;
using System.IO;

namespace INETS.ToolsLayer
{
    #region Using

    #endregion

    public class ImportExcelToCSV
    {
        #region Object Constructor
        public ImportExcelToCSV()
        { }
        #endregion

        #region Public Methods
        public void ImportExcel(string myExcelName)
        {
            FileStream stream = File.Open(myExcelName, FileMode.Open, FileAccess.Read);
            IExcelDataReader iExcelReader;
            iExcelReader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);
        }
        #endregion
    }
}
