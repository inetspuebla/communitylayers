﻿#region Descripcion
/*
    Clase       :   Helper
    Objetivo    :   Capa de Herramientas metodos diversos de ayuda
    Creador     :   Luis Vázquez C
    Fecha       :   01/08/2020
*/
#endregion

using System;
using System.Configuration;

namespace INETS.ToolsLayer
{
    public class Helper
    {
        public static string GetSetting(string sKey)
        {
            try
            {
                return ConfigurationManager.AppSettings[sKey].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetConnString(string sKey)
        {
            try
            {
                return ConfigurationManager.ConnectionStrings[sKey].ToString();
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2147467261)
                    return string.Empty;
                throw ex;
            }
        }
    }
}
