﻿using System.Net.Mail;

namespace INETS.ToolsLayer.Security
{
    public sealed class EventWin : INETS.ToolsLayer.EventWin
    {
        readonly string sSource = string.Empty;

        public EventWin(string _sSource) : base(_sSource)
        { }


        public override AlternateView GenerateBody(string sSaludo, string sContenido, string sDespedida)
        {
            return TMails.GenerateBody(sSaludo, sSaludo, sDespedida);
        }

        public Mail TMails
        {
            get
            {
                if (tMails == null)
                    tMails = new Mail("Modulo de seguridad");
                return tMails;
            }
        }

        public Mail tMails;
    }
}
