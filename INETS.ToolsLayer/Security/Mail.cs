﻿#region Descripcion
/*
    Clase       :   Mail
    Objetivo    :   Capa de Herramientas para manejo de correos clase heredada para Seguridad
    Creador     :   Luis Vázquez C
    Fecha       :   01/08/2020
*/
#endregion

namespace INETS.ToolsLayer.Security
{
    #region using
    using System;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    #endregion
    public class Mail : INETS.ToolsLayer.Mail
    {
        #region Object Constructors
        public Mail(string strProject) : base(strProject)
        { }
        #endregion
        public override AlternateView GenerateBody(string sSaludo, string sContenido, string sDespedida)
        {
            StringBuilder strBuild = new StringBuilder();

            try
            {
                StringBuilder sb = new StringBuilder();

                #region Declaracion de HTML
                sb.Append("<HTML>\n");
                sb.Append("<HEAD>\n");
                sb.Append("<TITLE>\n");
                sb.Append(Project + "\n");
                sb.Append("</TITLE>\n");
                sb.Append("<style type='text/css'>\n");
                sb.Append(".grid_Row {font-size:10px;font-family: Arial, Helvetica, Verdana; border-bottom:1px solid #7C7C94; border-left:1px solid #EDEDF5; border-right:1px solid #7C7C94; border-top:1px solid #EDEDF5; cursor:default; color:windowtext; white-space:nowrap; overflow :hidden; text-overflow:ellipsis; width:100px;} ");
                sb.Append("</style>\n");
                sb.Append("</HEAD>\n");
                sb.Append("<BODY bgcolor='b2cee4'>");
                sb.Append("<form id='correo' runat='server'>");
                #endregion

                #region Encabezado
                sb.Append("<table width='100%'  border='0' align='center' cellpadding='0' cellspacing='0' bordercolor='04478b'>")
                .Append("<tr height='20' valign='middle' align='center'>")
                .Append("<td width='100%' align='center'>")
                .Append("<div>")
                .Append("<h1>")
                .Append(Project)
                .Append("</h1>")
                .Append("</div>")
                .Append("</tr>")
                .Append("</table>")
                .Append("<br />")
                .Append("<br />");
                #endregion

                StringBuilder body = new StringBuilder();

                #region Body
                body.Append("<table width='820px'  border='0' align='center' cellpadding='0' cellspacing='0' bordercolor='04478b'>")
                        .Append("<tr height='20' valign='middle'>")
                        .Append("<td align='center' style='width:100%;'><strong><span style='font-size: small'>" + sSaludo + "</span></strong></td>");

                body.Append("<tr height='20'>")
                    .Append("   <td align='center' style='width:100%;'>")
                    .Append("       <br><br><br><br><span id='lblCot' style='font-size: small'>" + sContenido + "'</span>")
                    .Append("   </td>");
                body.Append("<tr height='20'>")
                    .Append("   <td align='center' style='width:100%;'>")
                    .Append("       <br><br><br><br><span id='lblCot' style ='font-size: 12;'><span style='text-decoration: underline'>" + sDespedida + "'</span>")
                    .Append("   </td>")
                    .Append(" </tr></table>");
                #endregion

                sb.Append(body.ToString());


                sb.Append("\n");
                sb.Append("</form>");
                sb.Append("</BODY>\n");
                sb.Append("</HTML>\n");

                AlternateView avwHTML = AlternateView.CreateAlternateViewFromString(sb.ToString(), null, MediaTypeNames.Text.Html);

                return avwHTML;
            }
            catch
            (Exception ex)
            {
                throw ex;
            }

        }
    }
}
