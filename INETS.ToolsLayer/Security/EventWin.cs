﻿using System.Net.Mail;

namespace INETS.ToolsLayer.SICOM
{
    public sealed class EventWin : INETS.ToolsLayer.EventWin
    {
        readonly string sSource = string.Empty;

        public EventWin(string _sSource) : base(_sSource)
        { }

        public void SendSoporte(string _Source)
        {
            SendSoporte(GenerateBody(this.Header, this.Content, this.Foot));
        }
        public override AlternateView GenerateBody(string sSaludo, string sContenido, string sDespedida)
        {
            return TMails.GenerateBody(sSaludo, sContenido, sDespedida);
        }

        public Mail TMails
        {
            get
            {
                if (tMails == null)
                    tMails = new Mail("SICOM");
                return tMails;
            }
        }

        public Mail tMails;
    }
}
