﻿#region Descripcion
/*
    Clase       :   Mail
    Objetivo    :   Capa de Herramientas para manejo de correos
    Creador     :   Luis Vázquez C
    Fecha       :   01/08/2020
*/
#endregion

namespace INETS.ToolsLayer
{
    #region using
    using System;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    #endregion
    public class Mail
    {
        string sProject = string.Empty;

        #region Object Constructor
        public Mail(string strProject)
        {
            sProject = strProject;
        }
        #endregion

        #region Public Methods
        public static SmtpClient AssembleSMTP()
        {
            try
            {
                string sHost = Helper.GetSetting("HostMail");
                int iPort = Convert.ToInt32(Helper.GetSetting("PortMail"));
                string sUserCred = Helper.GetSetting("UserCredentialMail");
                string sPassCred = Helper.GetSetting("PassCredentialMail");

                SmtpClient smtp = new SmtpClient("smtp.gmail.com, smtp.hotmail.com, smtp.yahoo.com, smtp.inets.com.mx")
                {
                    Host = sHost,
                    Port = iPort,
                    EnableSsl = false,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(sUserCred, sPassCred)
                };

                return smtp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MailAddress SetMailAddress()
        {
            try
            {
                string sAddress = Helper.GetSetting("AddressMail");
                string sName = Helper.GetSetting("NameAddressMail");

                return new MailAddress(sAddress, sName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MailAddress SetMailAddressSupport()
        {
            try
            {
                string sAddress = Helper.GetSetting("AddressMail");
                string sName = Helper.GetSetting("SupportNameMail");

                return new MailAddress(sAddress, sName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool MailTo(ref MailMessage pEmail, string pTo, string pCC, bool inBCC = false, bool isSupport = false)
        {
            try
            {
                bool sIsTesting = Convert.ToBoolean(Helper.GetSetting("IsTest"));

                if (isSupport && !sIsTesting)
                {
                    string sMailSupport = Helper.GetSetting("MailSoporte");
                    string[] arrayAddress = sMailSupport.Split(',');

                    foreach (string sTo in arrayAddress)
                    {
                        if (!ExistRecipient(pEmail, sTo))
                            pEmail.To.Add(sTo);
                    }
                }
                else if (sIsTesting)
                {
                    string sMailTest = Helper.GetSetting("MailTest");
                    string[] arrayAddress = sMailTest.Split(',');

                    foreach (string sTo in arrayAddress)
                    {
                        if (!ExistRecipient(pEmail, sTo))
                            pEmail.To.Add(sTo);
                    }
                }
                else
                {
                    string[] arrayAddress = pTo.Split(',');

                    if (pTo.Trim() != string.Empty)
                    {
                        foreach (string sTO in arrayAddress)
                        {
                            if (!ExistRecipient(pEmail, sTO))
                                pEmail.To.Add(sTO);
                        }
                    }

                    if (pCC.Trim() != string.Empty)
                    {
                        arrayAddress = pCC.Split(',');

                        foreach (string sCC in arrayAddress)
                        {
                            if (!ExistRecipient(pEmail, sCC))
                                pEmail.CC.Add(sCC);
                        }
                    }
                }

                if (inBCC && !isSupport && !sIsTesting)
                {
                    string sMailBCC = Helper.GetSetting("BCC");
                    string[] arrayAddress = sMailBCC.Split(',');

                    foreach (string sBCC in arrayAddress)
                    {
                        if (!ExistRecipient(pEmail, sBCC))
                            pEmail.Bcc.Add(sBCC);
                    }
                }

                if (pEmail.To.Count > 0 || pEmail.Bcc.Count > 0 || pEmail.CC.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual AlternateView GenerateBody(string sSaludo, string sContenido, string sDespedida)
        {
            StringBuilder strBuild = new StringBuilder();

            try
            {
                StringBuilder sb = new StringBuilder();

                #region Declaracion de HTML
                sb.Append("<HTML>\n");
                sb.Append("<HEAD>\n");
                sb.Append("<TITLE>\n");
                sb.Append(Project + "\n");
                sb.Append("</TITLE>\n");
                sb.Append("<style type='text/css'>\n");
                sb.Append(".grid_Row {font-size:10px;font-family: Arial, Helvetica, Verdana; border-bottom:1px solid #7C7C94; border-left:1px solid #EDEDF5; border-right:1px solid #7C7C94; border-top:1px solid #EDEDF5; cursor:default; color:windowtext; white-space:nowrap; overflow :hidden; text-overflow:ellipsis; width:100px;} ");
                sb.Append("</style>\n");
                sb.Append("</HEAD>\n");
                sb.Append("<BODY bgcolor='b2cee4'>");
                sb.Append("<form id='correo' runat='server'>");
                #endregion

                #region Encabezado
                sb.Append("<table width='100%'  border='0' align='center' cellpadding='0' cellspacing='0' bordercolor='04478b'>")
                .Append("<tr height='20' valign='middle' align='center'>")
                .Append("<td width='100%' align='center'>")
                .Append("<div>")
                .Append("<h1>")
                .Append(Project)
                .Append("</h1>")
                .Append("</div>")
                //.Append("<td width='50%'>" + "<IMG id='Logo border'='0' alt='Italpasta' src='cid: Logo' width ='24' height='24'/>" + "</td>")
                .Append("</tr>")
                .Append("</table>")
                .Append("<br />")
                .Append("<br />");
                #endregion

                StringBuilder body = new StringBuilder();

                #region Body
                body.Append("<table width='820px'  border='0' align='center' cellpadding='0' cellspacing='0' bordercolor='04478b'>")
                        .Append("<tr height='20' valign='middle'>")
                        .Append("<td align='center' style='width:100%;'><strong><span style='font-size: small'>" + sSaludo + "</span></strong></td>");

                body.Append("<tr height='20'>")
                    .Append("   <td align='center' style='width:100%;'>")
                    .Append("       <br><br><br><br><span id='lblCot' style='font-size: small'>" + sContenido + "'</span>")
                    .Append("   </td>");
                body.Append("<tr height='20'>")
                    .Append("   <td align='center' style='width:100%;'>")
                    .Append("       <br><br><br><br><span id='lblCot' style ='font-size: 12;'><span style='text-decoration: underline'>" + sDespedida + "'</span>")
                    .Append("   </td>")
                    .Append(" </tr></table>");
                #endregion

                sb.Append(body.ToString());


                sb.Append("\n");
                sb.Append("</form>");
                sb.Append("</BODY>\n");
                sb.Append("</HTML>\n");

                AlternateView avwHTML = AlternateView.CreateAlternateViewFromString(sb.ToString(), null, MediaTypeNames.Text.Html);

                return avwHTML;

            }
            catch
            (Exception ex)
            {
                throw ex;
            }

        }

        private static bool ExistRecipient(MailMessage pEmail, string pTo)
        {
            if (pEmail.To.Count > 0)
                foreach (MailAddress addreess in pEmail.To)
                {
                    if (addreess.Address.Trim() == pTo.Trim())
                        return true;
                }

            if (pEmail.CC.Count > 0)
                foreach (MailAddress addreess in pEmail.CC)
                {
                    if (addreess.Address.Trim() == pTo.Trim())
                        return true;
                }

            if (pEmail.Bcc.Count > 0)
                foreach (MailAddress addreess in pEmail.Bcc)
                {
                    if (addreess.Address.Trim() == pTo.Trim())
                        return true;
                }

            return false;
        }
        #endregion

        #region Public Properties
        public string Project
        {
            get { return sProject; }
            set { sProject = value; }
        }
        #endregion
    }
}
