﻿#region Descripcion
/*
    Clase       :   EventWin
    Objetivo    :   Capa de Herramientas para manejo de mensajes en Windows Event
    Creador     :   Luis Vázquez C
    Fecha       :   31/07/2020
*/
#endregion


namespace INETS.ToolsLayer
{
    using System;
    #region Using
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Net.Mime;
    #endregion

    public abstract class EventWin
    {
        string sSource = string.Empty;
        string sHeader = string.Empty;
        string sContent = string.Empty;
        string sFoot = string.Empty;
        string sBodyHTML = string.Empty;
        string sSubject = string.Empty;
        public EventWin(string _sSource)
        { sSource = _sSource; }

        #region Public Methods
        public void WriteMsg(string strMessage)
        {
            EventLog eventLog = new EventLog();
            if (!EventLog.SourceExists(SourceEvent))     //valida que exista tarea con ese nombre
            {
                EventLog.CreateEventSource(SourceEvent, "Application");// se crea un nuevo evento y el nuevo log
            }

            eventLog.Source = SourceEvent;
            eventLog.Log = "Application";

            eventLog.WriteEntry(strMessage);

        }

        public void WriteError(Exception pEx)
        {
            EventLog eventLog = new EventLog();
            string strMessage = "Error encontrado en " + pEx.Source;
            strMessage += ": " + pEx.Message.Trim();

            try
            {
                try
                {
                    strMessage += "<br>" + "<br>En: " + pEx.TargetSite.ToString().Trim();
                    strMessage += "<br>" + "<br>Detalle: " + pEx.StackTrace.ToString().Trim();

                    if (!EventLog.SourceExists(SourceEvent))     //valida que exista tarea con ese nombre
                    {
                        EventLog.CreateEventSource(SourceEvent, "Application");// se crea un nuevo evento y el nuevo log
                    }

                    eventLog.Source = SourceEvent;
                    eventLog.Log = "Application";

                    eventLog.WriteEntry(strMessage, EventLogEntryType.Error);
                }
                catch (Exception ex)
                {
                    strMessage += "<br><br>Adicional se dio un error al guardar en Windows Event: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }

                try
                {
                    MailMessage Email = new MailMessage();
                    using (SmtpClient smtp = Mail.AssembleSMTP())
                    {
                        Email.From = Mail.SetMailAddressSupport();

                        bool bExisTo = Mail.MailTo(ref Email, string.Empty.ToString(), string.Empty, false, true);

                        Email.Subject = Subject;
                        sHeader = Header +
                        "<br><br>" +
                        "El dia de hoy se registro un error en el proceso con la siguiente descripcion: " +
                        "<br><br>";
                        sContent = strMessage;
                        sFoot = Foot;

                        Email.IsBodyHtml = true;
                        AlternateView avwHTML = GenerateBody(sHeader, sContent, sFoot);

                        Email.AlternateViews.Add(avwHTML);
                        Email.Priority = MailPriority.High;

                        if (bExisTo)
                        {
                            //enviar correo
                            smtp.Send(Email);
                        }
                        else
                            throw new Exception("No existen destinatarios de correo de soporte tecnico");

                        Email.Dispose();
                        smtp.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    strMessage += "<br><br>Adicional se dio un error al enviar correo de soporte tecnico: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }
            }
            catch (Exception ex)
            {
                WriteMsg("Error en registro de mensaje de error." + "<br>" + "<br>" + ex.Message + "<br>" + "<br>" + ex.StackTrace);
            }
        }

        public void SendSoporte(MailMessage pEmail = null)
        {
            EventLog eventLog = new EventLog();
            try
            {
                try
                {
                    if (!EventLog.SourceExists(SourceEvent))     //valida que exista tarea con ese nombre
                    {
                        EventLog.CreateEventSource(SourceEvent, "Application");// se crea un nuevo evento y el nuevo log
                    }

                    eventLog.Source = SourceEvent;
                    eventLog.Log = "Application";

                    eventLog.WriteEntry(Content, EventLogEntryType.SuccessAudit);
                }
                catch (Exception ex)
                {
                    Content += "<br><br>Se presento un error al guardar en Windows Event: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }

                try
                {
                    MailMessage Email = new MailMessage();
                    if (pEmail != null)
                        Email = pEmail;

                    SmtpClient smtp = Mail.AssembleSMTP();
                    Email.From = Mail.SetMailAddressSupport();

                    bool bExisTo = Mail.MailTo(ref Email, string.Empty, string.Empty, false, true);

                    Email.Subject = Subject;

                    Email.IsBodyHtml = true;
                    AlternateView avwHTML = GenerateBody(Header, Content, Foot);

                    Email.AlternateViews.Add(avwHTML);
                    Email.Priority = MailPriority.High;

                    if (bExisTo)
                    {
                        //enviar correo
                        smtp.Send(Email);
                    }
                    else
                        throw new Exception("No existen destinatarios de correo de soporte tecnico");

                    Email.Dispose();
                    smtp.Dispose();
                }
                catch (Exception ex)
                {
                    Content += "<br><br>Adicional se dio un error al enviar correo de soporte tecnico: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }
            }
            catch (Exception ex)
            {
                WriteMsg("Error en registro de mensaje de error." + "<br>" + "<br>" + ex.Message + "<br>" + "<br>" + ex.StackTrace);
            }
        }

        public void SendSoporte(AlternateView  sBody, MailMessage pEmail = null)
        {
            EventLog eventLog = new EventLog();
            try
            {
                try
                {
                    if (!EventLog.SourceExists(SourceEvent))     //valida que exista tarea con ese nombre
                    {
                        EventLog.CreateEventSource(SourceEvent, "Application");// se crea un nuevo evento y el nuevo log
                    }

                    eventLog.Source = SourceEvent;
                    eventLog.Log = "Application";

                    eventLog.WriteEntry(Content, EventLogEntryType.SuccessAudit);
                }
                catch (Exception ex)
                {
                    Content += "<br><br>Se presento un error al guardar en Windows Event: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }

                try
                {
                    MailMessage Email = new MailMessage();
                    if (pEmail != null)
                        Email = pEmail;

                    SmtpClient smtp = Mail.AssembleSMTP();
                    Email.From = Mail.SetMailAddressSupport();

                    bool bExisTo = Mail.MailTo(ref Email, string.Empty, string.Empty, false, true);

                    Email.Subject = Subject;

                    Email.IsBodyHtml = true;
                    AlternateView avwHTML = sBody;

                    Email.AlternateViews.Add(avwHTML);
                    Email.Priority = MailPriority.High;

                    if (bExisTo)
                    {
                        //enviar correo
                        smtp.Send(Email);
                    }
                    else
                        throw new Exception("No existen destinatarios de correo de soporte tecnico");

                    Email.Dispose();
                    smtp.Dispose();
                }
                catch (Exception ex)
                {
                    Content += "<br><br>Adicional se dio un error al enviar correo de soporte tecnico: <br>"
                        + ex.Message + "<br>" + "<br>" + ex.StackTrace;
                }
            }
            catch (Exception ex)
            {
                WriteMsg("Error en registro de mensaje de error." + "<br>" + "<br>" + ex.Message + "<br>" + "<br>" + ex.StackTrace);
            }
        }
        #endregion
        #region Override Methods
        public virtual AlternateView GenerateBody(string sSaludo, string sContenido, string sDespedida)
        {
            AlternateView avwHTML = AlternateView.CreateAlternateViewFromString(sContenido, null, MediaTypeNames.Text.Html);
            return avwHTML;
        }
        #endregion

        #region AccessLayers

        public Mail TMail
        {
            get
            {
                if (tMail == null)
                    tMail = new Mail("SISTEMA DE COMISIONES ITALPASTA");
                return tMail;
            }
        }

        public Mail tMail;
        #endregion
        #region Public Properties
        public string SourceEvent
        {
            get { return sSource; }
            set { sSource = value; }
        }

        public string Header
        {
            get { return sHeader; }
            set { sHeader = value; }
        }

        public string Content
        {
            get { return sContent; }
            set { sContent = value; }
        }

        public string Foot
        {
            get { return sFoot; }
            set { sFoot = value; }
        }

        public string BodyHTML
        {
            get { return sBodyHTML; }
            set { sBodyHTML = value; }
        }

        public string Subject
        {
            get { return sSubject; }
            set { sSubject = value; }
        }
        #endregion
    }
}
