﻿#region Descripcion
/*
    Clase       :   Users
    Objetivo    :   Capa de Conexion a Base de Datos, provee Metodos de Conexion y Acceso a Base de Datos
    Creador     :   Luis Vazquez C.
    Fecha       :   14 de Julio, 2010
*/
#endregion


namespace INETS.DataAccess
{
    #region Using
    using System;
    using System.Data;
    using System.Data.Odbc;
    using System.Data.SqlClient;
    #endregion

    public class DbAccessSQL
    {
        public DbAccessSQL()
        {
            cnSQL = new SqlConnection(sConnSQL);
        }

        public DbAccessSQL(string pConnSQL)
        {
            _sConnSQL = pConnSQL;
            cnSQL = new SqlConnection(sConnSQL);
        }

        #region Variables
        public SqlConnection cnSQL;
        private SqlDataReader drAccess;
        private SqlDataAdapter daAccess;
        private SqlCommand cmSQL;

        private string _sConnSQL = INETS.ToolsLayer.Helper.GetConnString("administracion");
        private string sConnSQL
        {
            get { return _sConnSQL; }
            set { _sConnSQL = value; }
        }
        #endregion

        #region Execute Reader
        public SqlDataReader ExecuteReader(string strQuery)
        {
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                drAccess = cmSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public SqlDataReader ExecuteReader(string strQuery, SqlTransaction myTransact)
        {
            cnSQL = myTransact.Connection;

            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                drAccess = cmSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public SqlDataReader ExecuteReaderTransaction(string strQuery)
        {
            try
            {
                cnSQL.Open();
                SqlTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    drAccess = cmSQL.ExecuteReader();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public SqlDataReader ExecuteReader(string theServicio, SqlParameter[] theParameters, CommandType cmType = CommandType.StoredProcedure)
        {
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = theServicio;
                cmSQL.CommandType = cmType;

                for (int i = 0; i < theParameters.Length; i++)
                {
                    cmSQL.Parameters.Add(theParameters[i]);
                }

                drAccess = cmSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }
        #endregion

        #region Execute DataTable
        public DataTable ExecuteDataTable(string strQuery)
        {
            DataSet ds = new DataSet();
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;
                daAccess = new SqlDataAdapter
                {
                    SelectCommand = cmSQL
                };

                daAccess.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return ds.Tables[0];
        }
        #endregion

        #region Execute NonQuery
        public int ExecuteNonQuery(string strQuery)
        {
            int Result = 0;
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                Result = cmSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }

            return Result;
        }

        public int ExecuteNonQuery(string strQuery, SqlParameter[] theParameters)
        {
            int Result = 0;
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                for (int i = 0; i < theParameters.Length; i++)
                {
                    cmSQL.Parameters.Add(theParameters[i]);
                }

                Result = cmSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return Result;
        }

        public int ExecuteNonQuery(string strQuery, SqlTransaction myTransact)
        {
            cnSQL = myTransact.Connection;
            int Result = 0;
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                Result = cmSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return Result;
        }

        public int ExecuteNonQueryTransaction(string strQuery)
        {
            int Result = 0;
            try
            {
                cnSQL.Open();
                SqlTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    Result = cmSQL.ExecuteNonQuery();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return Result;
        }

        public int ExecuteNonQueryTransaction(string strQuery, SqlParameter[] theParameters)
        {
            try
            {
                int Result = 0;
                cnSQL.Open();

                SqlTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.StoredProcedure;

                    if (theParameters.Length > 0)
                    {
                        for (int i = 0; i < theParameters.Length; i++)
                        {
                            cmSQL.Parameters.Add(theParameters[i]);
                        }
                    }

                    Result = cmSQL.ExecuteNonQuery();
                    myTransact.Commit();

                    return Result;
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
        }
        #endregion

        #region Execute Scalar
        public object ExecuteScalar(string strQuery)
        {
            _ = new object();
            object obAccess;
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message + " " + sConnSQL);
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string theServicio, SqlParameter[] theParameters)
        {
            object obAccess = new object();
            try
            {
                cnSQL.Open();
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = theServicio;
                cmSQL.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < theParameters.Length; i++)
                {
                    cmSQL.Parameters.Add(theParameters[i]);
                }

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string strQuery, SqlTransaction myTransact)
        {
            cnSQL = myTransact.Connection;

            object obAccess = new object();
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string theServicio, SqlTransaction myTransact, SqlParameter[] theParameters)
        {
            cnSQL = myTransact.Connection;

            object obAccess = new object();
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = theServicio;
                cmSQL.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < theParameters.Length; i++)
                {
                    cmSQL.Parameters.Add(theParameters[i]);
                }

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalarTransaction(string strQuery)
        {
            object obAccess = new object();
            try
            {
                cnSQL.Open();
                SqlTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    obAccess = cmSQL.ExecuteScalar();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }
        #endregion

        #region Public Method
        public void Close()
        {
            if (cnSQL.State == ConnectionState.Open)
                cnSQL.Close();
        }
        #endregion
    }

    public class DbAccessODBCSQL
    {
        public DbAccessODBCSQL()
        {
            cnSQL = new OdbcConnection(sConnSQL);
            cnSQL.Open();
        }

        #region Variables
        private OdbcConnection cnSQL;
        private IDataReader drAccess;
        private OdbcDataAdapter daAccess;
        private OdbcCommand cmSQL;
        private readonly string sConnSQL = INETS.ToolsLayer.Helper.GetConnString("Server_SQL");
        #endregion

        #region Execute Reader
        public IDataReader ExecuteReader(string strQuery)
        {
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                drAccess = cmSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message + " " + sConnSQL);
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public IDataReader ExecuteReader(string strQuery, OdbcTransaction myTransact)
        {
            cnSQL = myTransact.Connection;

            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                drAccess = cmSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public IDataReader ExecuteReaderTransaction(string strQuery)
        {
            try
            {
                OdbcTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    drAccess = cmSQL.ExecuteReader();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }

        public IDataReader ExecuteReader(string theServicio, OdbcParameter[] theParameters)
        {
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = theServicio;
                cmSQL.CommandType = CommandType.Text;

                if (theParameters.Length > 0)
                {
                    for (int i = 0; i < theParameters.Length; i++)
                    {
                        cmSQL.Parameters.Add(theParameters[i]);
                    }

                    cmSQL.CommandType = CommandType.Text;
                    drAccess = cmSQL.ExecuteReader();
                }
                else
                {
                    drAccess = cmSQL.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("ERROR [42000] [Microsoft][ODBC SQL Server Driver][SQL Server]"))
                {
                    throw new Exception(ex.Message.Replace("ERROR [42000] [Microsoft][ODBC SQL Server Driver][SQL Server]", ""), new Exception(ex.Message));
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
            }
            return drAccess;
        }
        #endregion

        #region Execute DataTable
        public DataTable ExecuteDataTable(string strQuery)
        {
            DataSet ds = new DataSet();
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;
                daAccess = new OdbcDataAdapter
                {
                    SelectCommand = cmSQL
                };

                daAccess.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return ds.Tables[0];
        }
        #endregion

        #region Execute NonQuery
        public void ExecuteNonQuery(string strQuery)
        {
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                cmSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
        }

        public void ExecuteNonQuery(string strQuery, OdbcTransaction myTransact)
        {
            cnSQL = myTransact.Connection;

            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                cmSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
        }

        public void ExecuteNonQueryTransaction(string strQuery)
        {
            try
            {
                OdbcTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    cmSQL.ExecuteNonQuery();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
        }

        public int ExecuteNonQueryTransaction(string strQuery, OdbcParameter[] theParameters)
        {
            try
            {
                OdbcTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.StoredProcedure;

                    if (theParameters.Length > 0)
                    {
                        for (int i = 0; i < theParameters.Length; i++)
                        {
                            cmSQL.Parameters.Add(theParameters[i]);
                        }
                    }

                    int valor = cmSQL.ExecuteNonQuery();
                    myTransact.Commit();

                    return valor;
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
        }
        #endregion

        #region Execute Scalar
        public object ExecuteScalar(string strQuery)
        {
            _ = new object();
            object obAccess;
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;

                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message + " " + sConnSQL);
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string theServicio, SqlParameter[] theParameters)
        {
            object obAccess = new object();
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;

                cmSQL.CommandText = theServicio;
                cmSQL.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < theParameters.Length; i++)
                {
                    cmSQL.Parameters.Add(theParameters[i]);
                }

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string strQuery, OdbcTransaction myTransact)
        {
            cnSQL = myTransact.Connection;

            object obAccess = new object();
            try
            {
                cmSQL = cnSQL.CreateCommand();
                cmSQL.CommandTimeout = 0;
                cmSQL.Transaction = myTransact;
                cmSQL.CommandText = strQuery;
                cmSQL.CommandType = CommandType.Text;

                obAccess = cmSQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL != null)
                    if (cmSQL.Connection.State == ConnectionState.Open)
                        cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                    cnSQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalarTransaction(string strQuery)
        {
            object obAccess = new object();
            try
            {
                OdbcTransaction myTransact = cnSQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {
                    cmSQL = cnSQL.CreateCommand();
                    cmSQL.CommandTimeout = 0;
                    cmSQL.Transaction = myTransact;
                    cmSQL.CommandText = strQuery;
                    cmSQL.CommandType = CommandType.Text;

                    obAccess = cmSQL.ExecuteScalar();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    if (cnSQL.State == ConnectionState.Open)
                    {
                        myTransact.Rollback();
                    }
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmSQL.Connection.State == ConnectionState.Open)
                    cmSQL.Dispose();
                if (cnSQL.State == ConnectionState.Open)
                {
                    cnSQL.Close();
                }
            }
            return obAccess;
        }
        #endregion

        #region Public Method
        public void Close()
        {
            if (cnSQL.State == ConnectionState.Open)
                cnSQL.Close();
        }
        #endregion
    }

    public class DbAccessMySQL
    {
        #region Variables
        private OdbcConnection cnMySQL;
        private IDataReader drAccess;
        private OdbcCommand cmMySQL;
        private readonly DataSet dsMySQL = new DataSet();
        private readonly string sConnMySQL = INETS.ToolsLayer.Helper.GetConnString("Server_SQL");
        #endregion

        #region Execute Reader
        public IDataReader ExecuteReader(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                drAccess = cmMySQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
            }
            return drAccess;
        }

        public IDataReader ExecuteReader(string strQuery, OdbcTransaction myTransact)
        {
            cnMySQL = new OdbcConnection(sConnMySQL);
            cnMySQL.Open();
            try
            {
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.Transaction = myTransact;
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                drAccess = cmMySQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
            }
            return drAccess;
        }

        public IDataReader ExecuteReaderTransaction(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                OdbcTransaction myTransact = cnMySQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmMySQL = cnMySQL.CreateCommand();
                    cmMySQL.Transaction = myTransact;
                    cmMySQL.CommandText = strQuery;
                    cmMySQL.CommandType = CommandType.Text;

                    drAccess = cmMySQL.ExecuteReader();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
            }
            return drAccess;
        }
        #endregion

        #region Execute NonQuery
        public void ExecuteNonQuery(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                cmMySQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
        }

        public void ExecuteNonQuery(string strQuery, OdbcTransaction myTransact)
        {
            cnMySQL = new OdbcConnection(sConnMySQL);
            cnMySQL.Open();
            try
            {
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.Transaction = myTransact;
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                cmMySQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
        }

        public void ExecuteNonQueryTransaction(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                OdbcTransaction myTransact = cnMySQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmMySQL = cnMySQL.CreateCommand();
                    cmMySQL.Transaction = myTransact;
                    cmMySQL.CommandText = strQuery;
                    cmMySQL.CommandType = CommandType.Text;

                    cmMySQL.ExecuteNonQuery();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
        }
        #endregion

        #region Execute Scalar
        public object ExecuteScalar(string strQuery)
        {
            object obAccess = new object();
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                obAccess = cmMySQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalar(string strQuery, OdbcTransaction myTransact)
        {
            cnMySQL = new OdbcConnection(sConnMySQL);
            cnMySQL.Open();
            object obAccess = new object();
            try
            {
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.Transaction = myTransact;
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                obAccess = cmMySQL.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return obAccess;
        }

        public object ExecuteScalarTransaction(string strQuery)
        {
            object obAccess = new object();
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                OdbcTransaction myTransact = cnMySQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmMySQL = cnMySQL.CreateCommand();
                    cmMySQL.Transaction = myTransact;
                    cmMySQL.CommandText = strQuery;
                    cmMySQL.CommandType = CommandType.Text;

                    obAccess = cmMySQL.ExecuteScalar();
                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return obAccess;
        }
        #endregion

        #region Execute DataSet
        public DataSet ExecuteDataSet(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                OdbcDataAdapter daMy = new OdbcDataAdapter(cmMySQL);

                daMy.Fill(dsMySQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return dsMySQL;
        }

        public DataSet ExecuteDataSet(string strQuery, OdbcTransaction myTransact)
        {
            cnMySQL = new OdbcConnection(sConnMySQL);
            cnMySQL.Open();
            try
            {
                cmMySQL = cnMySQL.CreateCommand();
                cmMySQL.Transaction = myTransact;
                cmMySQL.CommandText = strQuery;
                cmMySQL.CommandType = CommandType.Text;

                OdbcDataAdapter daMy = new OdbcDataAdapter(cmMySQL);

                daMy.Fill(dsMySQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return dsMySQL;
        }

        public DataSet ExecuteDataSetTransaction(string strQuery)
        {
            try
            {
                cnMySQL = new OdbcConnection(sConnMySQL);
                cnMySQL.Open();
                OdbcTransaction myTransact = cnMySQL.BeginTransaction(IsolationLevel.ReadCommitted);
                try
                {


                    cmMySQL = cnMySQL.CreateCommand();
                    cmMySQL.Transaction = myTransact;
                    cmMySQL.CommandText = strQuery;
                    cmMySQL.CommandType = CommandType.Text;

                    OdbcDataAdapter daMy = new OdbcDataAdapter(cmMySQL);

                    daMy.Fill(dsMySQL);

                    myTransact.Commit();
                }
                catch (Exception ex)
                {
                    myTransact.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmMySQL != null)
                    if (cmMySQL.Connection.State == ConnectionState.Open)
                        cmMySQL.Dispose();
                if (cnMySQL.State == ConnectionState.Open)
                    cnMySQL.Close();
            }
            return dsMySQL;
        }
        #endregion
    }
}
