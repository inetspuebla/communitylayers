﻿

namespace INETS.Models.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public  class Menu
    {
        public int IdMenu { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Controller { get; set; }
        public string View { get; set; }
        public bool Public { get; set; }
        public int IdMenuParent { get; set; }
        public string Identifier { get; set; }
        public bool IsHeader { get; set; }
        public bool IsActive { get; set; }
        public bool IsMVC { get; set; }
    }
}
