﻿namespace INETS.Models.Security
{
    using System;
    public class User
    {
        public Guid IdGuid { get; set; }
        public string IdUser { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public Guid IdCreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastAccesDate { get; set; }
        public bool Locked { get; set; }
        public bool Active { get; set; }
        public DateTime LastChangeDate { get; set; }
        public int AccessErrorCounter { get; set; }
        public string Comments { get; set; }
        public bool IsAdmin { get; set; }
        public bool SuperAdmin { get; set; }
        public bool ChangePassword { get; set; }


    }
}
